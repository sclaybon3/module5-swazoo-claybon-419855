<?php
session_start(); 
require 'database.php';

ini_set('display_errors',1); 
error_reporting(E_ALL);


$username = $_SESSION['username'];

$id = $_POST['deleted'];



$stmt = $mysqli->prepare("DELETE from Event where id=?");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('s', $id);
$stmt->execute(); 
$stmt->close();

   echo json_encode(array(
		"success" => true
	));
	exit
?>