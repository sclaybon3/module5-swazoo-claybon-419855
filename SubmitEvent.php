<!doctype html>

<html lang="en">
<head>
	<Title>SubmitEvent</Title>
</head>
<body>

<?php
 
ini_set('display_errors',1); 
 error_reporting(E_ALL);

require 'database.php';
session_start();

if($_SESSION['token'] != $_POST['token']){
	die("Request forgery detected");
}

if(empty($_POST['Event']) && empty($_POST['Day'])){
     header("Location: login.php");
  }
$username = $_SESSION['username'];
if($_SESSION['username']=="guest"){
    die("Permission Denied");
}

$Title = $_POST['Title'];
$Event = $_POST['Event'];
$Day = $_POST['Day'];
$Username=$_SESSION['username'];
$Month = $_POST['month'];
$Year = $_POST['year'];

$stmt = $mysqli->prepare("insert into Event (Username, Event, Day, Month, Year, Title) values (?,?,?,?,?,?)");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('ssssss', $Username, $Event, $Day, $Month, $Year, $Title); 
$stmt->execute(); 
$stmt->close();

header("Location: Calendar-Ghost.php");

?>
</body>
